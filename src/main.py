import configparser

from annotation.display import Display

if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read('../config.ini', "utf8")

    Display('left', '../data', '../cropped_data', model_path='../models', model_number=0, config=config)
