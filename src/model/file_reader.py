from tensorflow.keras.preprocessing import image_dataset_from_directory
from tensorflow.keras.preprocessing.image import ImageDataGenerator


def get_labelled_data(data_path: str, im_height: int, im_width: int, data_augmentation: bool) -> tuple:
    if data_augmentation:
        datagen = ImageDataGenerator(
            validation_split=0.2,
            rotation_range=20,
            brightness_range=(0.0, 0.4),
            width_shift_range=0.2,
            height_shift_range=0.2,
            vertical_flip=True)

        train_generator = datagen.flow_from_directory(
            data_path,
            subset='training',
            target_size=(im_height, im_width),
            batch_size=32)
        validation_generator = datagen.flow_from_directory(
            data_path,
            subset='validation',
            target_size=(im_height, im_width),
            batch_size=32)

        return train_generator, validation_generator

    else:
        train_ds = image_dataset_from_directory(
            data_path,
            validation_split=0.2,
            subset='training',
            label_mode='categorical',
            seed=42,
            image_size=(im_height, im_width),
            batch_size=32)

        test_ds = image_dataset_from_directory(
            data_path,
            validation_split=0.2,
            subset='validation',
            label_mode='categorical',
            seed=42,
            image_size=(im_height, im_width),
            batch_size=32)

        return train_ds, test_ds
