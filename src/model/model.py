import numpy as np
from cv2 import resize

from tensorflow.keras import models, layers, Model, losses
from tensorflow.python.keras.callbacks import EarlyStopping, ModelCheckpoint


def build_on_top_of_pretrained_model(pretrained_model, n_classes: int, im_height: int, im_width: int, channels: int):
    model_input = layers.Input(shape=(im_height, im_width, channels))

    if channels == 1:
        base_model = layers.Flatten()(model_input)
        base_model = layers.RepeatVector(3)(base_model)
        base_model = layers.Reshape((im_height, im_width, 3))(base_model)
        base_model = pretrained_model(input_shape=(im_height, im_width, 3),
                                      weights='imagenet',
                                      include_top=False)(base_model)
        base_model.trainable = False

        head_model = layers.GlobalAveragePooling2D()(base_model)
        head_model = layers.Dense(128, activation='relu')(head_model)

        if n_classes == 2:
            head_model = layers.Dense(n_classes, activation='sigmoid')(head_model)
        else:
            head_model = layers.Dense(n_classes, activation='softmax')(head_model)

        model = Model(inputs=model_input, outputs=head_model)

    else:
        base_model = pretrained_model(input_shape=(im_height, im_width, 3),
                                      weights='imagenet',
                                      include_top=False)
        base_model.trainable = False

        head_model = layers.GlobalAveragePooling2D()(base_model.output)

        if n_classes == 2:
            head_model = layers.Dense(n_classes, activation='sigmoid')(head_model)
        else:
            head_model = layers.Dense(n_classes, activation='softmax')(head_model)

        model = Model(inputs=base_model.input, outputs=head_model)

    return model


def train_model(model, train_data, test_data, model_path: str, n_classes: int, optimizer, epochs: int):
    if n_classes == 2:
        loss = losses.BinaryCrossentropy(from_logits=True)
    else:
        loss = losses.CategoricalCrossentropy(from_logits=True)

    model.compile(optimizer=optimizer, loss=loss, metrics=['accuracy'])

    early_stop = EarlyStopping(monitor='val_loss', patience=5, mode='min', verbose=1)
    callbacks = [early_stop, ModelCheckpoint(model_path,
                                             monitor='val_loss',
                                             # monitor='accuracy',
                                             save_best_only=True,
                                             mode='min',
                                             # mode='max',
                                             verbose=1)]

    history = model.fit(train_data, validation_data=test_data,
                        batch_size=32, epochs=epochs, callbacks=callbacks)
    return model, history


def get_trained_model(model_path: str):
    model = models.load_model(model_path)
    return model


def get_top_n_predictions(image: np.ndarray, model, n: int) -> dict:
    top_n_predictions = {}
    if image is not None:
        image = resize(image, (224, 224))
        prediction = model.predict(image.reshape(1, image.shape[0], image.shape[1], 3))[0]
        top_n_predictions = get_top_n_results(prediction, n)
    return top_n_predictions


def get_top_n_results(prediction: np.ndarray, n: int) -> dict:
    top_n_predictions = {}
    for index in sorted(prediction.argsort()[::-1][:n]):
        top_n_predictions[index] = prediction[index]
    return top_n_predictions
