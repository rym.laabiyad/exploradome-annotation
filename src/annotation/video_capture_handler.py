import cv2


class VideoCaptureHandler:
    def __init__(self, left_or_right: str, config):
        self.left_or_right = left_or_right

        self.camera_id = int(config['ID'])
        self.width = int(config['WIDTH'])
        self.height = int(config['HEIGHT'])
        self.exposure = int(config['EXPOSURE'])
        self.contrast = int(config['CONTRAST'])
        self.brightness = int(config['BRIGHTNESS'])
        self.focus = int(config['FOCUS'])
        self.camera_crop_top = int(config['CAMERA_CROP_TOP'])
        self.camera_crop_bottom = int(config['CAMERA_CROP_BOTTOM'])
        self.camera_crop_left = int(config['CAMERA_CROP_LEFT'])
        self.camera_crop_right = int(config['CAMERA_CROP_RIGHT'])
        self.playarea_top = int(config['PLAYAREA_TOP'])
        self.playarea_bottom = int(config['PLAYAREA_BOTTOM'])
        self.playarea_left = int(config['PLAYAREA_LEFT'])
        self.playarea_right = int(config['PLAYAREA_RIGHT'])

        self.vid = None
        self.left_crop_dimensions = (
            self.camera_crop_top + self.playarea_left,
            self.height - self.camera_crop_bottom - self.playarea_right,
            self.camera_crop_left + self.playarea_bottom,
            int(self.width / 2) - self.playarea_top)
        self.right_crop_dimensions = (
            self.camera_crop_top + self.playarea_left,
            self.height - self.camera_crop_bottom - self.playarea_right,
            int(self.width / 2) + self.playarea_top,
            self.width - self.playarea_bottom - self.camera_crop_right)
        self.open_camera()

    def get_frame(self):
        if self.vid.isOpened():
            self.vid.set(cv2.CAP_PROP_AUTOFOCUS, 0)
            self.vid.set(cv2.CAP_PROP_FOCUS, self.focus)
            self.vid.set(cv2.CAP_PROP_EXPOSURE, self.exposure)
            self.vid.set(cv2.CAP_PROP_CONTRAST, self.contrast)
            self.vid.set(cv2.CAP_PROP_BRIGHTNESS, self.brightness)
            _, frame = self.vid.read()
            if frame is not None:
                if self.left_or_right == 'left':
                    left_frame = frame[self.left_crop_dimensions[0]:self.left_crop_dimensions[1],
                                       self.left_crop_dimensions[2]:self.left_crop_dimensions[3]]
                    return left_frame
                else:
                    right_frame = frame[self.right_crop_dimensions[0]:self.right_crop_dimensions[1],
                                        self.right_crop_dimensions[2]:self.right_crop_dimensions[3]]
                    return right_frame

    def open_camera(self):
        self.vid = cv2.VideoCapture(self.camera_id)

        if not self.vid.isOpened():
            raise ValueError('Unable to open self.video source')

        self.vid.set(cv2.CAP_PROP_FRAME_WIDTH, self.width)
        self.vid.set(cv2.CAP_PROP_FRAME_HEIGHT, self.height)

        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def __del__(self):
        if self.vid is not None and self.vid.isOpened():
            self.vid.release()
