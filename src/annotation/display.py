import tkinter
import cv2
from PIL import ImageTk, Image
from datetime import datetime
import os

from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2
from tensorflow.keras.optimizers import Adam

from annotation.video_capture_handler import VideoCaptureHandler
from model.file_reader import get_labelled_data
from model.model import build_on_top_of_pretrained_model, train_model
from utils.image_processor import ImageProcessor
from utils.utils import create_directory_if_does_not_exist


class Display:
    def __init__(self, left_or_right: str, data_path: str, cropped_data_path: str, model_path: str, model_number: int,
                 config):
        self.window = tkinter.Tk()
        self.left_or_right = left_or_right
        self.data_path = data_path
        self.cropped_data_path = cropped_data_path
        self.model_path = model_path
        self.model_nb = model_number
        self.camera_config = config['CAMERA']

        self.video_capture = VideoCaptureHandler(self.left_or_right, self.camera_config)

        self.window_width = 1280

        self.top_canvas = tkinter.Canvas(self.window, width=self.window_width, height=40, bg='white')
        self.top_canvas.pack(side=tkinter.TOP)

        self.canvas = tkinter.Canvas(self.window, width=self.window_width, height=800, bg='white')
        self.canvas.pack(side=tkinter.BOTTOM)

        self.title = self.top_canvas.create_text(10, 10, text='Label:', font=('Arial', 14), fill='black',
                                                 anchor=tkinter.NW)
        self.label = tkinter.Entry(self.top_canvas)
        self.label.place(anchor=tkinter.NW, height=30, width=100, x=80, y=10)

        self.btn_film = tkinter.Button(self.top_canvas, text='Start filming', command=self.start_filming)
        self.btn_film.place(anchor=tkinter.NW, height=30, width=200, x=200, y=10)

        self.btn_stop = tkinter.Button(self.top_canvas, text='Stop', command=self.stop)
        self.btn_stop.place(anchor=tkinter.NW, height=30, width=60, x=440, y=10)

        self.btn_take_picture = tkinter.Button(self.top_canvas, text='Take picture', command=self.take_picture)
        self.btn_take_picture.place(anchor=tkinter.NW, height=30, width=200, x=540, y=10)

        self.btn_train = tkinter.Button(self.top_canvas, text='Train model', command=self.train_model)
        self.btn_train.place(anchor=tkinter.NW, height=30, width=200, x=780, y=10)

        self.tk_preview_live = self.canvas.create_image(0, 0, image=None, anchor=tkinter.NW)
        self.tk_preview_segmented = self.canvas.create_image(400, 0, image=None, anchor=tkinter.NW)
        self.tk_preview_to_classify = self.canvas.create_image(400, 250, image=None, anchor=tkinter.NW)

        self.filming = False

        self.directory = None
        self.cropped_directory = None

        self.delay = 15
        self.preview_live = None
        self.preview_segmented = None
        self.preview_to_classify = None
        self.raw_frame = None
        self.image_to_classify = None
        self.update()

        self.window.mainloop()

    def update(self):
        frame = self.video_capture.get_frame()
        if frame is not None:
            self.update_frame(frame)

        self.window.after(self.delay, self.update)

    def start_filming(self):
        self.filming = True

    def stop(self):
        self.filming = False

    def take_picture(self):
        if self.image_to_classify is not None:
            self.directory = os.path.join(self.data_path, self.label.get())
            self.cropped_directory = os.path.join(self.cropped_data_path, self.label.get())
            create_directory_if_does_not_exist(self.directory)
            create_directory_if_does_not_exist(self.cropped_directory)
            frame_name = f'frame-{datetime.now().strftime("%d-%m-%Y-%H-%M-%S.%f")}'
            print('Saving', frame_name)
            cv2.imwrite(os.path.join(self.directory, f'{frame_name}.jpg'), self.raw_frame)
            cv2.imwrite(os.path.join(self.cropped_directory, f'{frame_name}_cropped.jpg'),
                        self.image_to_classify)
        else:
            print('No frame available')

    def update_frame(self, frame):
        image_processor = ImageProcessor(frame, False, 224, 224)
        image_processor.process_image()
        if image_processor.image_to_classify is not None:
            self.btn_film.config(state="normal")
            self.btn_take_picture.config(state="normal")
            image_processor.image_to_classify = cv2.rotate(image_processor.image_to_classify,
                                                           cv2.ROTATE_90_CLOCKWISE)
            self.raw_frame = frame
            self.image_to_classify = image_processor.image_to_classify
            if self.filming:
                self.take_picture()
        else:
            self.image_to_classify = None
            self.btn_film.config(state="disabled")
            self.btn_take_picture.config(state="disabled")

        self.preview_live = ImageTk.PhotoImage(image=Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)))
        self.canvas.itemconfigure(self.tk_preview_live, image=self.preview_live)

        if image_processor.segmented_and_cropped_image is not None:
            self.preview_segmented = ImageTk.PhotoImage(
                image=Image.fromarray(image_processor.segmented_and_cropped_image))
            self.canvas.itemconfigure(self.tk_preview_segmented, image=self.preview_segmented)

        if image_processor.image_to_classify is not None:
            self.preview_to_classify = ImageTk.PhotoImage(image=Image.fromarray(image_processor.image_to_classify))
            self.canvas.itemconfigure(self.tk_preview_to_classify, image=self.preview_to_classify)

    def train_model(self):
        train_ds, test_ds = get_labelled_data(self.cropped_data_path, 224, 224, data_augmentation=True)

        n_classes = 12

        model_path = os.path.join(self.model_path, f'model_{n_classes}classes_{self.model_nb}')
        self.model_nb += 1

        model = build_on_top_of_pretrained_model(MobileNetV2, n_classes, 224, 224, 3)
        opt = Adam(lr=1e-3, decay=1e-5)

        train_model(model, train_ds, test_ds, model_path, n_classes, opt, epochs=30)

        self.update()
