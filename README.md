# <span style="font-family: Arial Black;">Trangr<span style="color: #fdc441;">I</span><span style="color: #f97738;">A</span>m</span> Annotation 

This tool is used to help training a new Computer Vision model for the [TangrIAm project](https://gitlab.com/Exploradome/TangrIAm).

<span style="font-family: Arial Black;">Trangr<span style="color: #fdc441;">I</span><span style="color: #f97738;">A</span>m</span> Annotation features:
- capture of individual images for a label (tangram figure)
- capture of a batch of images for a label (tangram figure)
- train a model based on MobileNetV2

# Insights
In main.py file, an instance of class Display is created with the following arguments :
- The word 'left' or 'right', to capture either the left or the right half of the camera (only the table is used)
- The path where the original pictures should be stored
- The path where the cropped pictures (will be used for training) should be stored
- The video source (Usually 0, 1 or 2)
- The model path

The folders where the pictures and the models will be stored should be created in advance

# Remarks
The training of the model should only be launched when we have pictures of all 12 figures